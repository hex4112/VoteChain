package fr.insalyon.h4112.controller;

import com.sun.org.apache.regexp.internal.RE;
import fr.insalyon.h4112.Service.AuthorityService;
import fr.insalyon.h4112.Utility.ResultMapFactory;
import fr.insalyon.h4112.dao.AuthorityDao;
import fr.insalyon.h4112.model.Authority;
import fr.insalyon.h4112.model.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qifan on 2018/5/2.
 */

@RestController
public class AuthorityController {
    @Autowired
    AuthorityService authorityService;

    @RequestMapping(value="/authority", method = { RequestMethod.POST})
    @ResponseBody
    public Map<String,Object> saveAuthority(String login,String password){
        Map<String,Object> map=new HashMap<String,Object>();
        Authority authority=null;



        try {
            authority=authorityService.registerAuthority(login,password);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return ResultMapFactory.getErrorResultMap("Inscription echoue, veuillez reessayer");
        }
        if(authority!=null) {
            map.put("Authority", authority);
            return ResultMapFactory.getSuccessResultMap(map);
        }
        return ResultMapFactory.getErrorResultMap("Inscription echoue, veuillez reessayer");
    }

    @RequestMapping(value = "/sessionAuth", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> login(String login, String password, HttpSession session) {
        Map<String, Object> map = new HashMap<String, Object>();

        Authority authority=authorityService.loginAuthority(login,password);
        if (authority != null) {
            map.put("authority", authority);
            session.setAttribute("authority", authority);
            session.setAttribute("type", "authority");
            return ResultMapFactory.getSuccessResultMap(map);
        }
        return ResultMapFactory.getErrorResultMap("password erreur");
    }

    @RequestMapping(value = "/check/{id}", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> checkVoter(int status, @PathVariable Integer id) {

        try {if (status==1) {
            authorityService.VerifyAndAccept(id);
        } else {
            authorityService.VerifyAndReject(id);
        }
        return ResultMapFactory.getSuccessResultMap("");
        } catch (Exception e) {
            return ResultMapFactory.getErrorResultMap("");
        }


    }


}
