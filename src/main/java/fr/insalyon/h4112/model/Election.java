package fr.insalyon.h4112.model;

import javax.persistence.*;
import java.util.*;

/**
 * Created by siyingjiang on 2018/4/25.
 */
@Entity
@Table(name = "election")
public class Election {
    private Integer id;
    private String name;
    private Date startTime;
    private Date endTime;
    private List<Candidate> candidates;
    private Set<PubKey> pubKeys;
    private Integer type; //1 for simple, 2 for delegation, 3 for condorcet, 4 for majoritaire
    private String description;
    private String address;
    private String creatorAddress;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ELECTION_ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    @Column(name = "ELECTION_NAME", precision = 6)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ELECTION_START_TIME", precision = 6)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Column(name = "ELECTION_END_TIME", precision = 6)
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @OneToMany(fetch=FetchType.EAGER)
    //@JoinColumn(name="CANDIDATE_ID", unique= true, nullable=true, insertable=true, updatable=true)
    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="PUBKEY_ID", unique= false, nullable=true, insertable=true, updatable=true)
    public Set<PubKey> getPubKeys() {
        return pubKeys;
    }


    public void setPubKeys(Set<PubKey> pubKeys) {
        this.pubKeys = pubKeys;
    }
    public void addPubKey (PubKey pubKey) {
        pubKeys.add(pubKey);
    }
    public Election() {
    }

    public void addCandidate(Candidate candidate) {
        candidates.add(candidate);
    }


    @Column(name = "ELECTION_TYPE" ,nullable=false)
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "ELECTION_DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "ELECTION_ADDRESS",unique=true)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "ELECTION_CREATORADDRESS")
    public String getCreatorAddress() {
        return creatorAddress;
    }

    public void setCreatorAddress(String creatorAddress) {
        this.creatorAddress = creatorAddress;
    }

    public Election(String name, Date startTime, Date endTime, Integer type, String description, String address,String creatorAddress) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = type;
        this.description = description;
        this.address = address;
        this.creatorAddress=creatorAddress;
        this.candidates=new ArrayList<>();
        this.pubKeys=new HashSet<>();
    }
}
