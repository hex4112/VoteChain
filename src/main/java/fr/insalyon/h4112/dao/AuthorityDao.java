package fr.insalyon.h4112.dao;

import fr.insalyon.h4112.Utility.HibernateUtil;
import fr.insalyon.h4112.model.Authority;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuthorityDao extends BaseDao{
    /**
     * persist the authority entity to the data base
     * @param authority
     */
    public void save (Authority authority) {
        //TODO:generate exception when duplicated value

        this.getSession().save(authority);
    }

    public Authority findById (Integer id) {
        return (Authority)this.getSession().get(Authority.class,id);

    }

    public Authority login (String login,String hash) {
        Authority a;
        String query="SELECT a FROM Authority a WHERE a.login=:login";
        Query query1=this.getSession().createQuery(query);
        query1.setParameter("login",login);
        List<Authority> result= (List<Authority>) query1.list();
        if(!result.isEmpty()) {
            a=(Authority) query1.list().get(0);
        } else {
            System.out.println("login does not exist");
            return null;
        }
        if (a.getHashPassword().equals(hash)){
            return a;
        } else {
            System.out.println("password error");
            return null;
        }


    }
}
