package fr.insalyon.h4112.Service;

import fr.insalyon.h4112.dao.AuthorityDao;
import fr.insalyon.h4112.dao.VoterDao;
import fr.insalyon.h4112.model.Authority;
import fr.insalyon.h4112.model.Voter;
import org.hibernate.NonUniqueObjectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by qifan on 2018/5/2.
 */

@Service
@Transactional
public class AuthorityService {
    @Autowired
    AuthorityDao authorityDao;
    @Autowired
    VoterDao voterDao;

    public Authority registerAuthority (String login, String password) throws NonUniqueObjectException {
        String pHash=""+(password.hashCode());

        Authority authority=new Authority(login,pHash);
        try {
            authorityDao.save(authority);
        } catch (NonUniqueObjectException noe) {
            throw noe;
        }
        return authority;
    }

    public Authority loginAuthority (String login, String password) {
        return  authorityDao.login(login,password.hashCode()+"");
    }

    public void VerifyAndAccept (Integer idVoter) {
        Voter voter=voterDao.findById(idVoter);
        voter.setStatus(1);
        voterDao.update(voter);
    }

    public void VerifyAndReject (Integer idVoter) {
        Voter voter=voterDao.findById(idVoter);
        voter.setStatus(2);
        voterDao.update(voter);
    }
}
