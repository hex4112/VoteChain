Truffle Framework

Truffle is a development environment, testing framework and asset pipeline for Ethereum, aiming to make life as an Ethereum developer easier.
With Truffle, we get:
Built-in smart contract compilation, linking, deployment and binary management.

Install
$ npm install -g truffle

Usage
in VoteChain/truffle
we can run : truffle compile
the compiled contract/Election.sol is now in truffle/buid/contract/Election.json

we use the Election.json in order to get the .abi  and .bytecode of the contract that are used by web3js(Ethereum compatible JavaScript API which implements the Generic JSON RPC spec)
.abi : application binary interface
.bytecode : google


Note: contracts .json are already in the webapp of the application server.
if the conrtracts are changed then 1 run : truffle compile
                                   2 transfer .json to /VoteChain/src/main/webapp/Ressources