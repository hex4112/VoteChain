pragma solidity ^0.4.17;
contract Election {

    struct Voter {
        uint weight;
        bool voted;
        uint8 vote;
    }
    struct Proposal {
        uint voteCount;
    }

    address authority;
    mapping(address => Voter) voters;
    Proposal[] proposals;

    /// Create a new ellection with $(_numProposals) different proposals.
    constructor(uint8 _numProposals) public {
        authority = msg.sender;
        proposals.length = _numProposals;
    }

    /// Give $(toVoter) the right to vote on this ballot.
    /// May only be called by $(chairperson).
    function giveRightToVote(address toVoter) public {
        if (msg.sender != authority || voters[toVoter].voted) return;
        voters[toVoter].weight = 1;
    }

    /// Give a single vote to proposal $(toProposal).
    function vote(uint8 toProposal) public {
        Voter storage sender = voters[msg.sender];
        if (sender.voted || toProposal >= proposals.length) return;
        sender.voted = true;
        sender.vote = toProposal;
        proposals[toProposal].voteCount += sender.weight;
    }
    
    /// return the wining Proposal
    function winningProposal() public constant returns (uint8 _winningProposal) {
        uint256 winningVoteCount = 0;
        for (uint8 prop = 0; prop < proposals.length; prop++)
            if (proposals[prop].voteCount > winningVoteCount) {
                winningVoteCount = proposals[prop].voteCount;
                _winningProposal = prop;
            }
    }
    
    /// return the voteCount for a _numProposal
    function votesForProposal(uint _numProposal) public constant returns (uint _index, uint _voteCount) {
        _index=_numProposal;
		_voteCount=proposals[_numProposal].voteCount;
    }
    
}