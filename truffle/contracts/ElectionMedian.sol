pragma solidity ^0.4.0;
contract ElectionMedian {

    struct Voter {
        bool voted;
        uint[] marks;
    }
    struct Proposal {
        uint[6] marks;
    }

    address authority;
    mapping(address => Voter) voters;
    Proposal[] proposals;

    /// Create a new ellection with $(_numProposals) different proposals.
    constructor(uint8 _numProposals) public {
        authority = msg.sender;
        proposals.length = _numProposals;
    }

    /// Give $(toVoter) the right to vote on this ballot.
    /// May only be called by $(chairperson).
    function giveRightToVote(address toVoter) public {
        if (msg.sender != authority || voters[toVoter].voted) return;
        
    }

    /// Give marks to all proposals
    function vote(uint[] givenMark) public {
        Voter storage sender = voters[msg.sender];
        if (sender.voted || givenMark.length != proposals.length) return;
        sender.voted = true;
        sender.marks = givenMark;
        for (uint8 prop = 0; prop < proposals.length; prop++) {
            if (givenMark[prop]>5) continue;
            proposals[prop].marks[givenMark[prop]]++;
        }
        
    }
    
    /// return the voteCount for a specific Proposal
    function nbMarksFor(uint8 numProposal, uint8 mark) 
    public constant returns (uint8 _numProposal, uint8 _mark,uint _votes) {
        _numProposal=numProposal;
        _mark=mark;
        _votes=proposals[numProposal].marks[mark];
    }
    
}